﻿namespace Codefarts.MeshMaps
{
    using System;

    using Codefarts.MageStormEditor.Scripts;

    using UnityEngine;

    public interface IMapObjectVertexData
    {
        Guid UniqueId { get; }
        Vector3[] Vertexes { get; }
        Vector3[] Normals { get; }
        Vector2[] TextureCoordinates { get; }

      //  BoxCollider Collider { get; set; }

        Bounds Bounds { get; set; }
        Material[] Materials { get; }

        int SubMeshCount { get; }

        int VertexCount { get; }
        int TriangleCount { get; }

        int[] GetIndicies(int subMeshIndex);
    }
}
