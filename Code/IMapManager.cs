﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.MeshMaps
{
    using UnityEngine;

    public interface IMapManager
    {
        void BeginBulkEditing();

        void EndBulkEditing();
        bool IsBulkEditing { get; }
        int Count(int x, int y);
        object[] Get(int x, int y);
        void Remove(int x, int y, int layer);
        void Set(int x, int y, int layer, object value);
        void Add(int x, int y, object value);
                                               
        IMapObjectVertexData[] Raycast(Ray ray, bool all);
    }
}
