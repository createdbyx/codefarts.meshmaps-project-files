// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.MeshMaps
{
    using System;
    using System.Collections.Generic;

    using UnityEngine;

    public abstract class GridMapManagerBase : MonoBehaviour, IMapManager
    {
        public abstract void BeginBulkEditing();

        public abstract void EndBulkEditing();

        public abstract bool IsBulkEditing { get; }

        public abstract int Count(int x, int y);

        public abstract object[] Get(int x, int y);

        public abstract void Remove(int x, int y, int layer);

        public abstract void Set(int x, int y, int layer, object value);

        public abstract void Add(int x, int y, object value);

        public abstract IMapObjectVertexData[] Raycast(Ray ray, bool all);

        /*
        public BlockModel StackBlock(int x, int y)
        {
#if PERFORMANCE
            var perf = PerformanceTesting<string>.Instance;
            perf.Start(PerformanceConstants.MageStormMap_StackBlock);
#endif

            var callback = new Func<int, int, int, BlockModel>(
                (locationX, locationY, layerIndex) =>
                {
                    //var blockName = string.Format("Block_{0} {1}x{2}", layerIndex, locationX, locationY);
                    //var newBlockModel = GameManager.Instance.NewBlock(blockName, new Vector3(locationX, 0, locationY));
                    var newBlockModel = new BlockModel();
                    newBlockModel.LocationX = locationX;
                    newBlockModel.LocationY = locationY;

                    float minY;
                    float maxY;
                    var stack = this.Get(locationX, locationY);
                    Code.Helpers.CalculateHeight(stack, out minY, out maxY);
                    var height = newBlockModel.Height;
                    maxY = maxY == float.MinValue ? 0 : maxY;

                    newBlockModel.SetProperties(maxY + height, maxY);
                    this.Add(locationX, locationY, newBlockModel);

                    return newBlockModel;
                });

            try
            {
                BlockModel newBlock = null;
                var stack = this.Get(x, y);
                if (stack != null)
                {
                    newBlock = callback(x, y, stack.Length);
                }

                return newBlock;
            }
            finally
            {
#if PERFORMANCE
                perf.Stop(PerformanceConstants.MageStormMap_StackBlock);
#endif
            }
        }
        */
    }
}