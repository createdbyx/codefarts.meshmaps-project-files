// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.MeshMap
{
    using System.Collections.Generic;

    using Codefarts.Imaging;
    using Codefarts.MageStormEditor;
    using Codefarts.MeshMaps;

    using UnityEditorInternal;

    using UnityEngine;

    public class SingleMeshChunkManager : GridMapManagerBase
    {
        public int chunkSize = 16;
        public int MaxLayers = 4;
        private GenericImage<SingleMeshGridMapManager> chunks;
        public GameObject meshParentGameObject;
        public GameObject collidersGameObject;
        public GameObject selectionBoundsGameObject;

        Object[] emptyObjectsArray = new Object[0];

        private bool isBulkEditing;

        /// <summary>
        /// Start is called just before any of the Update methods is called the first time.
        /// </summary>
        public void Start()
        {
            this.chunks = new GenericImage<SingleMeshGridMapManager>(this.MapWidth / this.chunkSize, this.MapHeight / this.chunkSize);
            this.meshParentGameObject = this.meshParentGameObject == null ? this.gameObject : this.meshParentGameObject;

            for (var y = 0; y < this.chunks.Height; y++)
            {
                for (var x = 0; x < this.chunks.Width; x++)
                {
                    var newGameObject = new GameObject(string.Format("Chunk {0}x{1}", x, y));
                    newGameObject.transform.parent = this.meshParentGameObject.transform;
                    //  newGameObject.transform.position = new Vector3(x * this.chunkSize, 0, y * this.chunkSize);
                    var manager = newGameObject.AddComponent<SingleMeshGridMapManager>();
                    manager.MapWidth = this.chunkSize;
                    manager.MapHeight = this.chunkSize;
                    manager.MaxLayers = this.MaxLayers;
                    // manager.collidersGameObject = this.collidersGameObject;
                    //  manager.selectionBoundsGameObject = this.selectionBoundsGameObject;
                    this.chunks[x, y] = manager;
                }
            }
        }

        public int MapHeight = 16;

        public int MapWidth = 16;

        public override void BeginBulkEditing()
        {
            for (var y = 0; y < this.chunks.Height; y++)
            {
                for (var x = 0; x < this.chunks.Width; x++)
                {
                    this.chunks[x, y].BeginBulkEditing();
                }
            }

            this.isBulkEditing = true;
        }

        public override void EndBulkEditing()
        {
            for (var y = 0; y < this.chunks.Height; y++)
            {
                for (var x = 0; x < this.chunks.Width; x++)
                {
                    this.chunks[x, y].EndBulkEditing();
                    var filter = this.chunks[x, y].GetComponent<MeshFilter>();
                    var collider = this.chunks[x, y].GetComponent<MeshCollider>();
                    collider.sharedMesh = filter.sharedMesh;
                }
            }

            this.isBulkEditing = false;
        }

        public override bool IsBulkEditing
        {
            get
            {
                return this.isBulkEditing;
            }
        }

        public override int Count(int x, int y)
        {
            // calc chunk
            var chunkX = x / this.chunkSize;
            var chunkY = y / this.chunkSize;
            var chunk = this.chunks[chunkX, chunkY];
            if (chunk == null)
            {
                return 0;
            }

            var chunkIndexX = x - (chunkX * this.chunkSize);
            var chunkIndexY = y - (chunkY * this.chunkSize);
            return chunk.Count(chunkIndexX, chunkIndexY);
        }

        public override object[] Get(int x, int y)
        {
            // calc chunk
            var chunkX = x / this.chunkSize;
            var chunkY = y / this.chunkSize;
            var chunk = this.chunks[chunkX, chunkY];
            if (chunk == null)
            {
                return this.emptyObjectsArray;
            }

            var chunkIndexX = x - (chunkX * this.chunkSize);
            var chunkIndexY = y - (chunkY * this.chunkSize);

            return chunk.Get(chunkIndexX, chunkIndexY);
        }

        public override void Remove(int x, int y, int layer)
        {
            // calc chunk
            var chunkX = x / this.chunkSize;
            var chunkY = y / this.chunkSize;
            var chunk = this.chunks[chunkX, chunkY];
            if (chunk == null)
            {
                return;
            }

            var chunkIndexX = x - (chunkX * this.chunkSize);
            var chunkIndexY = y - (chunkY * this.chunkSize);
            chunk.Remove(chunkIndexX, chunkIndexY, layer);
        }

        public override void Set(int x, int y, int layer, object value)
        {
            // calc chunk
            var chunkX = x / this.chunkSize;
            var chunkY = y / this.chunkSize;
            var chunk = this.chunks[chunkX, chunkY];
            if (chunk == null)
            {
                return;
            }

            var chunkIndexX = x - (chunkX * this.chunkSize);
            var chunkIndexY = y - (chunkY * this.chunkSize);
            chunk.Set(chunkIndexX, chunkIndexY, layer, value);
        }

        public override void Add(int x, int y, object value)
        {
            // calc chunk
            var chunkX = x / this.chunkSize;
            var chunkY = y / this.chunkSize;
            var chunk = this.chunks[chunkX, chunkY];

            var chunkIndexX = x - (chunkX * this.chunkSize);
            var chunkIndexY = y - (chunkY * this.chunkSize);
            chunk.Add(chunkIndexX, chunkIndexY, value);
        }

        public override IMapObjectVertexData[] Raycast(Ray ray, bool all)
        {
            var list = new List<IMapObjectVertexData>();
            var bounds = new Bounds();
            for (var y = 0; y < this.chunks.Height; y++)
            {
                for (var x = 0; x < this.chunks.Width; x++)
                {
                    bounds.center = new Vector3((x * this.chunkSize) + (this.chunkSize / 2), 0, (y * this.chunkSize) + (this.chunkSize / 2));
                    bounds.size = new Vector3(this.chunkSize, float.MaxValue, this.chunkSize);
                    if (bounds.IntersectRay(ray))
                    {
                        var items = this.chunks[x, y].Raycast(ray, all);
                        if (items != null && items.Length > 0)
                        {
                            list.AddRange(items);
                        }
                    }
                }
            }

            return list.ToArray();
        }
    }
}