// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.MeshMap
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;

    using Codefarts.Imaging;
    using Codefarts.ExtensionMethods;
    using Codefarts.MageStormEditor;
    using Codefarts.MeshMaps;

    using UnityEngine;

    [RequireComponent(typeof(MeshFilter), typeof(MeshRenderer), typeof(MeshCollider))]
    public class SingleMeshGridMapManager : GridMapManagerBase
    {
        private GenericImage<List<object>> layers;
        public int MaxLayers = 4;

        private Mesh mapMesh;

        private int newSubMeshIndex;

        //  public GameObject collidersGameObject;

        //    public GameObject selectionBoundsGameObject;

        private Dictionary<int, MaterialDataModel> materials;
        private Dictionary<Guid, int> meshIndexes;

        //private float maxY;
        //private float minY;
        public event EventHandler UpdatedRendererMaterials;
        private Dictionary<Guid, IMapObjectVertexData> objectTracker = new Dictionary<Guid, IMapObjectVertexData>();

        public Guid[] TrackingObjects
        {
            get
            {
                var keys = new Guid[this.objectTracker.Keys.Count];
                this.objectTracker.Keys.CopyTo(keys, 0);
                return keys;
            }
        }

        public IMapObjectVertexData GetTrackingObject(Guid uniqueId)
        {
            return this.objectTracker[uniqueId];
        }

        [SerializeField]
        private int mapWidth;

        [SerializeField]
        private int mapHeight;

        public int MapHeight
        {
            get
            {
                return this.mapHeight;
            }

            set
            {
                this.mapHeight = value;
                var image = this.layers;
                if (image != null)
                {
                    image.Height = value;
                }
            }
        }

        public int MapWidth
        {
            get
            {
                return this.mapWidth;
            }

            set
            {
                this.mapWidth = value;
                var image = this.layers;
                if (image != null)
                {
                    image.Width = value;
                }
            }
        }

        protected virtual void OnUpdatedRendererMaterials()
        {
            var handler = this.UpdatedRendererMaterials;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        public GenericImage<List<object>> GetLayers()
        {
            return this.layers;
        }

        private Material[] materialArray;

        //Vector3[] vertexNormals = new Vector3[]
        //            {
        //                new Vector3(0f, -1f, 0f),     // bottom  0
        //                new Vector3(0f, -1f, 0f),     // bottom  1
        //                new Vector3(0f, -1f, 0f),     // bottom  2
        //                new Vector3(0f, -1f, 0f),     // bottom  3
        //                new Vector3(0f, 1f, 0f),      // top     4
        //                new Vector3(0f, 1f, 0f),      // top     5
        //                new Vector3(0f, 1f, 0f),      // top     6
        //                new Vector3(0f, 1f, 0f),      // top     7
        //                new Vector3(0f,  0f, -1f),    // back    8
        //                new Vector3(0f,  0f, -1f),    // back    9
        //                new Vector3(0f,  0f, -1f),    // back    10
        //                new Vector3(0f,  0f, -1f),    // back    11
        //                new Vector3(-1f, 0f, 0f),     // left    12
        //                new Vector3(-1f, 0f, 0f),     // left    13
        //                new Vector3(-1f, 0f, 0f),     // left    14
        //                new Vector3(-1f, 0f, 0f),     // left    15
        //                new Vector3(0f,  0f, 1f),     // front   16
        //                new Vector3(0f,  0f, 1f),     // front   17
        //                new Vector3(0f,  0f, 1f),     // front   18
        //                new Vector3(0f,  0f, 1f),     // front   19
        //                new Vector3(1f,  0f, 0f),     // right   20
        //                new Vector3(1f,  0f, 0f),     // right   21
        //                new Vector3(1f,  0f, 0f),     // right   22
        //                new Vector3(1f,  0f, 0f),     // right   23
        //            };

        public int MaterialDataCount
        {
            get
            {
                return this.materials == null ? 0 : this.materials.Count;
            }
        }

        public int StartIndexCount
        {
            get
            {
                return this.meshIndexes.Count;
            }
        }

        public KeyValuePair<int, MaterialDataModel>[] GetMaterialData()
        {
            if (this.materials == null)
            {
                return new KeyValuePair<int, MaterialDataModel>[0];
            }

            var items = new KeyValuePair<int, MaterialDataModel>[this.materials.Count];
            var index = 0;
            foreach (var pair in this.materials)
            {
                items[index] = new KeyValuePair<int, MaterialDataModel>(pair.Key, pair.Value);
                index++;
            }

            return items;
        }

        public IEnumerable<KeyValuePair<Guid, int>> GetStartIndexes()
        {
            foreach (var pair in this.meshIndexes)
            {
                yield return pair;
            }
        }

        public Material[] GetMaterialsArray()
        {
            if (this.materialArray == null)
            {
                return new Material[0];
            }

            var items = new Material[this.materialArray.Length];
            this.materialArray.CopyTo(items, 0);
            return items;
        }

        public void Start()
        {
            this.materialArray = new Material[0];
            this.layers = new GenericImage<List<object>>(this.mapWidth, this.mapHeight);
            this.materials = new Dictionary<int, MaterialDataModel>();
            this.meshIndexes = new Dictionary<Guid, int>();
            this.mapMesh = new Mesh();
            var filter = this.GetComponent<MeshFilter>();
            filter.sharedMesh = this.mapMesh;
            //this.collidersGameObject = this.collidersGameObject == null ? this.gameObject : this.collidersGameObject;
            //   this.selectionBoundsGameObject = this.selectionBoundsGameObject == null ? this.gameObject : this.selectionBoundsGameObject;
            //this.collidersGameObject = this.collidersGameObject == null ? new GameObject("Colliders") : this.collidersGameObject;
            //this.selectionBoundsGameObject = this.selectionBoundsGameObject == null ? new GameObject("Bounds") : this.selectionBoundsGameObject;
        }

        public override void BeginBulkEditing()
        {
            this.isBulkEditing = true;
        }

        public override void EndBulkEditing()
        {
            this.isBulkEditing = false;
        }

        public override bool IsBulkEditing
        {
            get
            {
                return this.isBulkEditing;
            }
        }

        public override int Count(int x, int y)
        {
            var stack = this.layers[x, y];
            if (stack == null)
            {
                stack = new List<object>();
                this.layers[x, y] = stack;
            }

            return stack.Count;
        }

        public int GetNewSubMeshIndex()
        {
            return this.newSubMeshIndex++;
        }

        public override object[] Get(int x, int y)
        {
            var stack = this.layers[x, y];
            if (stack == null)
            {
                stack = new List<object>();
                this.layers[x, y] = stack;
            }

            return stack.ToArray();
        }

        public override void Remove(int x, int y, int layer)
        {
            var stack = this.layers[x, y];
            if (stack == null)
            {
                stack = new List<object>();
                this.layers[x, y] = stack;
            }

            // we will not remove mesh data but we will update the indexes
            var vertexData = stack[layer] as IMapObjectVertexData;
            if (vertexData != null)
            {
                this.RemoveVertexData(vertexData);
                this.objectTracker.Remove(vertexData.UniqueId);
            }


            //var vertexData = stack[layer] as IMapObjectVertexData;
            //if (vertexData != null)
            //{
            //    this.objectTracker.Remove(vertexData.UniqueId);
            //}

            // remove the item from the stack
            stack.RemoveAt(layer);
        }

        private void RemoveVertexData(IMapObjectVertexData vertexData)
        {
            if (vertexData == null || !this.meshIndexes.ContainsKey(vertexData.UniqueId))
            {
                return;
            }

            var changed = vertexData as INotifyPropertyChanged;
            var changing = vertexData as INotifyPropertyChanging;
            if (changed != null)
            {
                changed.PropertyChanged -= this.BlockPropertyChanged;
            }

            if (changing != null)
            {
                changing.PropertyChanging -= this.BlockPropertyChanging;
            }

            // get the start index for the block
            var startIndex = this.meshIndexes[vertexData.UniqueId];

            // get vertex information
            var vertexes = this.mapMesh.vertices;
            var uvs = this.mapMesh.uv;
            var normals = this.mapMesh.normals;

            // remove vertex information
            var vertexCount = vertexData.VertexCount;
            vertexes = vertexes.RemoveRange(startIndex, vertexCount);
            uvs = uvs.RemoveRange(startIndex, vertexCount);
            normals = normals.RemoveRange(startIndex, vertexCount);

            // set new vertex information
            this.mapMesh.triangles = new int[0];
            this.mapMesh.vertices = vertexes;
            this.mapMesh.uv = uvs;
            this.mapMesh.normals = normals;

            // shift start indexes
            var indexKeys = new Guid[this.meshIndexes.Keys.Count];
            this.meshIndexes.Keys.CopyTo(indexKeys, 0);
            foreach (var key in indexKeys)
            {
                var value = this.meshIndexes[key];
                if (value > startIndex)
                {
                    this.meshIndexes[key] = value - vertexCount;
                }
            }

            // remove block start index entry
            this.meshIndexes.Remove(vertexData.UniqueId);

            // remove reference to block from materials
            var keys = new int[this.materials.Keys.Count];
            this.materials.Keys.CopyTo(keys, 0);
            foreach (var key in keys)
            {
                var value = this.materials[key];
                //var removed = value.FloorIndexes.Remove(vertexData.UniqueId) |
                //              value.WallIndexes.Remove(vertexData.UniqueId) |
                //              value.CeilingIndexes.Remove(vertexData.UniqueId);
                var removed = value.Indexes.Remove(vertexData.UniqueId);

                // check if removed
                if (removed)
                {
                    this.RemoveMaterialData(key);
                }
            }

            // destroy collider and selection bounds components
            //if (vertexData.Collider != null)
            //{
            //    DestroyImmediate(vertexData.Collider);
            //}

            //if (vertexData.Bounds != null)
            //{
            //    DestroyImmediate(vertexData.Bounds);
            //}

            // TODO: Boo! Hiss! Special case for blocks a big no no
            var block = vertexData as BlockModel;
            if (block != null)
            {
                // dispose of any attached shapes
                if (block.FloorShape != null)
                {
                    DestroyImmediate(block.FloorShape);
                }

                if (block.CeilingShape != null)
                {
                    DestroyImmediate(block.CeilingShape);
                }
            }

            // rebuild the indicies
            this.RebuildIndicies();

            // update renderer materials
            this.renderer.sharedMaterials = this.materialArray;
            this.OnUpdatedRendererMaterials();

            // recalculate the bounds
            this.mapMesh.RecalculateBounds();
        }

        /*
             private void RemoveBlock(BlockModel block)
        {
            if (block == null || !this.meshIndexes.ContainsKey(block.UniqueId))
            {
                return;
            }

            // we no longer need to be hooked into the model events
            block.PropertyChanged -= this.BlockPropertyChanged;
            block.PropertyChanging -= this.BlockPropertyChanging;

            // get the start index for the block
            var startIndex = this.meshIndexes[block.UniqueId];

            // get vertex information
            var vertexes = this.mapMesh.vertices;
            var uvs = this.mapMesh.uv;
            var normals = this.mapMesh.normals;

            // remove vertex information
            vertexes = vertexes.RemoveRange(startIndex, 24);
            uvs = uvs.RemoveRange(startIndex, 24);
            normals = normals.RemoveRange(startIndex, 24);

            // set new vertex information
            this.mapMesh.triangles = new int[0];
            this.mapMesh.vertices = vertexes;
            this.mapMesh.uv = uvs;
            this.mapMesh.normals = normals;

            // shift start indexes
            var indexKeys = new Guid[this.meshIndexes.Keys.Count];
            this.meshIndexes.Keys.CopyTo(indexKeys, 0);
            foreach (var key in indexKeys)
            {
                var value = this.meshIndexes[key];
                if (value > startIndex)
                {
                    this.meshIndexes[key] = value - 24;
                }
            }

            // remove block start index entry
            this.meshIndexes.Remove(block.UniqueId);

            // remove reference to block from materials
            var keys = new int[this.materials.Keys.Count];
            this.materials.Keys.CopyTo(keys, 0);
            foreach (var key in keys)
            {
                var value = this.materials[key];
                var removed = value.FloorIndexes.Remove(block.UniqueId) |
                              value.WallIndexes.Remove(block.UniqueId) |
                              value.CeilingIndexes.Remove(block.UniqueId);

                // check if removed
                if (removed)
                {
                    this.RemoveMaterialData(key);
                }
            }

            // destroy collider and selection bounds components
            if (block.Collider != null)
            {
                DestroyImmediate(block.Collider);
            }

            if (block.Bounds != null)
            {
                DestroyImmediate(block.Bounds);
            }

            // dispose of any attached shapes
            if (block.FloorShape != null)
            {
                DestroyImmediate(block.FloorShape);
            }

            if (block.CeilingShape != null)
            {
                DestroyImmediate(block.CeilingShape);
            }

            // rebuild the indicies
            this.RebuildIndicies();

            // update renderer materials
            this.renderer.sharedMaterials = this.materialArray;
            this.OnUpdatedRendererMaterials();

            // recalculate the bounds
            this.mapMesh.RecalculateBounds();
        }

         */

        private void RemoveMaterialData(int key)
        {
            if (!this.materials.ContainsKey(key))
            {
                return;
            }

            var value = this.materials[key];

            // check if material is no longer used and if not remove it and remove the entry from the materials array
            if (value.Indexes.Count == 0)
            {
                // remove the entry from the materials dictionary
                this.materials.Remove(key);

                // process all materials and update material indexes if they are greater then the material index that we just removed
                foreach (var pair in this.materials)
                {
                    if (pair.Value.MaterialIndex > value.MaterialIndex)
                    {
                        pair.Value.MaterialIndex--;
                    }
                }

                // remove the material from the material array
                this.materialArray = this.materialArray.RemoveRange(value.MaterialIndex, 1);
            }
        }

        /*
            private void RemoveMaterialData(int key)
        {
            if (!this.materials.ContainsKey(key))
            {
                return;
            }

            var value = this.materials[key];

            // check if material is no longer used and if not remove it and remove the entry from the materials array
            if (value.FloorIndexes.Count == 0 && value.WallIndexes.Count == 0 && value.CeilingIndexes.Count == 0)
            {
                // remove the entry from the materials dictionary
                this.materials.Remove(key);

                // process all materials and update material indexes if they are greater then the material index that we just removed
                foreach (var pair in this.materials)
                {
                    if (pair.Value.MaterialIndex > value.MaterialIndex)
                    {
                        pair.Value.MaterialIndex--;
                    }
                }

                // remove the material from the material array
                this.materialArray = this.materialArray.RemoveRange(value.MaterialIndex, 1);
            }
        }
         */

        public override void Set(int x, int y, int layer, object value)
        {
            var stack = this.layers[x, y];
            if (value == null)
            {
                var changed = stack[layer] as INotifyPropertyChanged;
                var changing = stack[layer] as INotifyPropertyChanging;
                if (changed != null)
                {
                    changed.PropertyChanged -= this.BlockPropertyChanged;
                }

                if (changing != null)
                {
                    changing.PropertyChanging -= this.BlockPropertyChanging;
                }

                //var data = stack[layer] as BlockModel;
                //if (data != null)
                //{
                //    data.PropertyChanged -= this.BlockPropertyChanged;
                //    data.PropertyChanging -= this.BlockPropertyChanging;
                //}
            }

            stack[layer] = value;
        }

        public override void Add(int x, int y, object value)
        {
            // get the stack for the given map position
            var stack = this.layers[x, y];

            // if the stack does not exit create it
            if (stack == null)
            {
                stack = new List<object>();
                this.layers[x, y] = stack;
            }

            // don't add if reached max layers
            if (stack.Count >= this.MaxLayers)
            {
                return;
            }

            // add model to the stack
            stack.Add(value);

            // get mesh vertex interface reference   
            var vertexData = value as IMapObjectVertexData;

            // if value does not implement the vertex interface we can just exit here
            if (vertexData == null)
            {
                return;
            }

            // if the model has already been added we can just exit
            if (this.meshIndexes.ContainsKey(vertexData.UniqueId))
            {
                return;
            }

            // check for vertex over flow
            if (this.mapMesh.vertexCount + vertexData.VertexCount > 65000)
            {
                throw new OverflowException("Can't add to map will result in too many vertexes.");
            }


            // add value to the object tracker
            this.objectTracker.Add(vertexData.UniqueId, vertexData);

            //// setup components for collider and selection bounds
            //if (this.collidersGameObject != null)
            //{
            //    vertexData.Collider = this.collidersGameObject.AddComponent<BoxCollider>();
            //}

            //if (this.selectionBoundsGameObject != null)
            //{
            //    var selectionBounds = this.selectionBoundsGameObject.AddComponent<SelectionBounds>();
            //    selectionBounds.enabled = false;
            //    selectionBounds.useCustomBounds = true;
            //    var bounds = vertexData.Bounds;
            //    selectionBounds.center = bounds.center;
            //    selectionBounds.size = bounds.size;
            //}

            // record the start index for the new mesh data
            var startIndex = this.mapMesh.vertexCount;
            this.meshIndexes.Add(vertexData.UniqueId, startIndex);

            // get vertex data 
            var vertexes = vertexData.Vertexes;
            var uvs = vertexData.TextureCoordinates;
            var vertexNormals = vertexData.Normals;

            // add the new vertex data to the mesh
            this.mapMesh.vertices = this.mapMesh.vertices.Add(vertexes);
            this.mapMesh.uv = this.mapMesh.uv.Length == 0
                                  ? this.mapMesh.uv.Add(uvs)
                                  : this.mapMesh.uv.Replace(this.mapMesh.vertexCount - uvs.Length, uvs);
            this.mapMesh.normals = this.mapMesh.normals.Length == 0
                                       ? this.mapMesh.normals.Add(vertexNormals)
                                       : this.mapMesh.normals.Replace(this.mapMesh.vertexCount - vertexNormals.Length, vertexNormals);

            //foreach (var vertex in vertexes)
            //{
            //    this.maxY = Math.Max(this.maxY, vertex.y);
            //    this.minY = Math.Max(this.minY, vertex.y);
            //}

            // add materials for each sub mesh of the vertex data
            for (var i = 0; i < vertexData.SubMeshCount; i++)
            {
                var material = vertexData.Materials[i];
                if (material != null)
                {
                    var data = this.UpdateMeshIndices(material);
                    var count = data.Indexes.ContainsKey(vertexData.UniqueId) ? data.Indexes[vertexData.UniqueId] : 0;
                    data.Indexes[vertexData.UniqueId] = count + 1;
                    this.rebuildIndexes = true;
                }
            }

            var block = value as BlockModel;
            if (block != null)
            {
                this.PositionShapes(block);
                this.UpdateShapeRotations(block);
                this.ScaleShape(block.FloorShape);
                this.ScaleShape(block.CeilingShape);
            }

            var changed = vertexData as INotifyPropertyChanged;
            var changing = vertexData as INotifyPropertyChanging;
            if (changed != null)
            {
                changed.PropertyChanged += this.BlockPropertyChanged;
            }

            if (changing != null)
            {
                changing.PropertyChanging += this.BlockPropertyChanging;
            }
        }

        /*
        public override IMapObjectVertexData[] Raycast(Ray ray, bool all)
        {
#if PERFORMANCE
            var perf = PerformanceTesting<string>.Instance;
            perf.Start(PerformanceConstants.SingleMeshGridMapManager_Raycast);
#endif

            // TODO: This code is horrible! If called by third parties (in game) the ray cast is checked against 1024 tracked objects
            // (If this manager is part of a chunked map 16x16x4 that is 1024 checks which is too slow.)
            var list = new List<IMapObjectVertexData>();
            foreach (var trackingObject in this.objectTracker)
            {
                var bounds = trackingObject.Value.Bounds;
                var properties = trackingObject.Value as IPropertiesCollection;
                if (properties != null)
                {
                    var locationX = properties.Properties.ContainsName("LocationX") ? properties.Properties.Get<int>("LocationX") : 0;
                    var locationY = properties.Properties.ContainsName("LocationY") ? properties.Properties.Get<int>("LocationY") : 0;
                    bounds.center += new Vector3(locationX, 0, locationY);
                }

                 
                //if (!all)
                //{
                //    // we break up the ray into smaller X number of rays as a cheat to help resolve depth issues related to wheather or not we only want
                //    // to return the first collision  
                //    // var segmentLength = (ray.direction - ray.origin).magnitude  ;
                //    var vector = ray.direction - ray.origin;
                //    vector = vector.normalized * vector.magnitude;
                //    //  var vectorLength = vector * segmentLength;
                //    var foundCollision = false;
                //    for (var i = 0f; i < 1f; i += 0.01f)
                //    {
                //        var newRay = new Ray(ray.origin, ray.origin + (vector * i));
                //        if (bounds.IntersectRay(newRay))
                //        {
                //            list.Add(trackingObject.Value);
                //            foundCollision = true;
                //            break;
                //        }
                //    }

                //    // if found collision break out of the object tracking loop
                //    if (foundCollision)
                //    {
                //        break;
                //    }
                //}
                //else   
                if (bounds.IntersectRay(ray))
                {
                    list.Add(trackingObject.Value);
                    if (!all)
                    {
                        break;
                    }
                }
            }

#if PERFORMANCE
            perf.Stop(PerformanceConstants.SingleMeshGridMapManager_Raycast);
#endif

            return list.ToArray();
        }
        */


        public override IMapObjectVertexData[] Raycast(Ray ray, bool all)
        {
            var hit = new RaycastHit[1];
            if (all)
            {
                hit = Physics.RaycastAll(ray);
            }
            else
            {
                if (!Physics.Raycast(ray, out hit[0], Mathf.Infinity))
                {
                    return null;
                }


            }


            // TODO: This code is horrible! If called by third parties in game the ray cast is checked against 1024 tracked objects
            // (If this manager is part of a chunked map 16x16x4 that is 1024 checks which is too slow.)
            var list = new List<IMapObjectVertexData>();

            foreach (var rayHit in hit)
            {
                // check if we hit our transform and if not return null.
                if (rayHit.transform.GetInstanceID() != this.transform.GetInstanceID())
                {
                    continue;
                }

                // get vertex index
                var vertexIndex = this.mapMesh.triangles[rayHit.triangleIndex * 3];

                var foundMatch = false;

                // get the vertex
                foreach (var obj in this.objectTracker)
                {
                    var startIndex = this.meshIndexes[obj.Key];
                    var vertexCount = obj.Value.VertexCount;
                    if (vertexIndex >= startIndex && vertexIndex < startIndex + vertexCount)
                    {
                        // we found a object collision so add it to the list
                        list.Add(obj.Value);
                        if (!all)
                        {
                            foundMatch = true;       
                            break;
                        }
                    }
                }

                if (!all && foundMatch)
                {
                    break;
                }
            }

            return list.ToArray();
        }

        /// <summary>
        /// Update is called every frame, if the MonoBehaviour is enabled.
        /// </summary>
        public void Update()
        {
            if (this.rebuildIndexes && !this.isBulkEditing)
            {
                //Debug.Log("Rebuilding indicies");
                this.RebuildIndicies();

#if PERFORMANCE
                var perf = PerformanceTesting<string>.Instance;
                perf.Start(PerformanceConstants.SingleMeshGridMapManager_RecalculateBounds);
#endif
                // update bounds data for mesh
                this.mapMesh.RecalculateBounds();
#if PERFORMANCE
                perf.Stop(PerformanceConstants.SingleMeshGridMapManager_RecalculateBounds);
#endif
                this.rebuildIndexes = false;
            }
        }


        private Dictionary<string, object> blockPropertyChange = new Dictionary<string, object>();

        private bool rebuildIndexes;

        private bool isBulkEditing;

        private void BlockPropertyChanging(object sender, PropertyChangingEventArgs e)
        {
            var block = sender as BlockModel;
            if (block == null)
            {
                return;
            }

            switch (e.PropertyName)
            {
                case "FloorTop":
                    this.blockPropertyChange[e.PropertyName] = block.FloorTop;
                    break;

                case "FloorBottom":
                    this.blockPropertyChange[e.PropertyName] = block.FloorBottom;
                    break;

                case "WallUvScale":
                    this.blockPropertyChange[e.PropertyName] = block.WallUvScale;
                    break;

                case "WallUvOffset":
                    this.blockPropertyChange[e.PropertyName] = block.WallUvOffset;
                    break;

                case "Floor":
                    this.blockPropertyChange[e.PropertyName] = block.Floor;
                    break;

                case "Walls":
                    this.blockPropertyChange[e.PropertyName] = block.Walls;
                    break;

                case "Ceiling":
                    this.blockPropertyChange[e.PropertyName] = block.Ceiling;
                    break;

                case "CeilingShape":
                    this.blockPropertyChange[e.PropertyName] = block.CeilingShape;
                    break;

                case "CeilingShapeRotation":
                    this.blockPropertyChange[e.PropertyName] = block.CeilingShapeRotation;
                    break;

                case "FloorShape":
                    this.blockPropertyChange[e.PropertyName] = block.FloorShape;
                    break;

                case "FloorShapeRotation":
                    this.blockPropertyChange[e.PropertyName] = block.FloorShapeRotation;
                    break;
            }
        }

        private void BlockPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (!this.blockPropertyChange.ContainsKey(e.PropertyName))
            {
                return;
            }

            var block = sender as BlockModel;
            if (block == null)
            {
                return;
            }

            int startIndex;
            Vector3[] vertexes;
            Material previousMaterial;
            switch (e.PropertyName)
            {
                case "FloorTop":
                    vertexes = this.mapMesh.vertices;
                    startIndex = this.meshIndexes[block.UniqueId];

                    var floorIndexes = new int[]
                        {
                            startIndex + 4, startIndex + 5, startIndex + 6, startIndex + 7,     // top
                            startIndex + 9, startIndex + 11,   // back
                            startIndex + 13, startIndex + 15,   // left
                            startIndex + 17, startIndex + 19,   // front
                            startIndex + 21, startIndex + 23,   // right
                        };

                    var topValue = block.FloorTop;
                    foreach (var i in floorIndexes)
                    {
                        vertexes[i].y = topValue;
                    }

                    this.mapMesh.vertices = vertexes;
                    this.mapMesh.RecalculateBounds();
                    this.PositionShapes(block);
                    break;

                case "FloorBottom":
                    vertexes = this.mapMesh.vertices;
                    startIndex = this.meshIndexes[block.UniqueId];

                    var bottomIndexes = new int[]
                        {
                            startIndex + 0, startIndex + 1, startIndex + 2,  startIndex + 3,    // bottom
                            startIndex + 8, startIndex + 10,   // back
                            startIndex + 12, startIndex + 14,   // left
                            startIndex + 16, startIndex + 18,   // front
                            startIndex + 20, startIndex + 22,   // right
                        };

                    var bottomValue = block.FloorBottom;
                    foreach (var i in bottomIndexes)
                    {
                        vertexes[i].y = bottomValue;
                    }

                    this.mapMesh.vertices = vertexes;
                    this.mapMesh.RecalculateBounds();
                    this.PositionShapes(block);
                    break;

                case "WallUvScale":
                case "WallUvOffset":
                    var uvs = this.mapMesh.uv;

                    var wallScaleX = block.WallUvScale.x;
                    var wallScaleY = block.WallUvScale.y;
                    var wallOffsetX = block.WallUvOffset.x;
                    var wallOffsetY = block.WallUvOffset.y;

                    var newWallUvs = new Vector2[]                      
                    {                                       
                        new Vector2((1f * wallScaleX) - wallOffsetX, (0f * wallScaleY) - wallOffsetY),         // 8 back
                        new Vector2((0f * wallScaleX) - wallOffsetX, (1f * wallScaleY) - wallOffsetY),         // 9 back
                        new Vector2((0f * wallScaleX) - wallOffsetX, (0f * wallScaleY) - wallOffsetY),         // 10 back
                        new Vector2((1f * wallScaleX) - wallOffsetX, (1f * wallScaleY) - wallOffsetY),         // 11 back
                                                                                                               
                        new Vector2((1f * wallScaleX) - wallOffsetX, (0f * wallScaleY) - wallOffsetY),         // 12 left
                        new Vector2((0f * wallScaleX) - wallOffsetX, (1f * wallScaleY) - wallOffsetY),         // 13 left
                        new Vector2((0f * wallScaleX) - wallOffsetX, (0f * wallScaleY) - wallOffsetY),         // 14 left
                        new Vector2((1f * wallScaleX) - wallOffsetX, (1f * wallScaleY) - wallOffsetY),         // 15 left
                                                                                                               
                        new Vector2((1f * wallScaleX) - wallOffsetX, (0f * wallScaleY) - wallOffsetY),         // 16 front
                        new Vector2((0f * wallScaleX) - wallOffsetX, (1f * wallScaleY) - wallOffsetY),         // 17 front
                        new Vector2((0f * wallScaleX) - wallOffsetX, (0f * wallScaleY) - wallOffsetY),         // 18 front
                        new Vector2((1f * wallScaleX) - wallOffsetX, (1f * wallScaleY) - wallOffsetY),         // 19 front
                                                                                                               
                        new Vector2((1f * wallScaleX) - wallOffsetX, (0f * wallScaleY) - wallOffsetY),         // 20 right
                        new Vector2((0f * wallScaleX) - wallOffsetX, (1f * wallScaleY) - wallOffsetY),         // 21 right
                        new Vector2((0f * wallScaleX) - wallOffsetX, (0f * wallScaleY) - wallOffsetY),         // 22 right
                        new Vector2((1f * wallScaleX) - wallOffsetX, (1f * wallScaleY) - wallOffsetY),         // 23 right   
                    };

                    startIndex = this.meshIndexes[block.UniqueId];

                    Array.Copy(newWallUvs, 0, uvs, startIndex + 8, newWallUvs.Length);
                    this.mapMesh.uv = uvs;
                    break;

                case "Floor":
                    previousMaterial = (Material)this.blockPropertyChange[e.PropertyName];
                    // this.HandleMaterialChanged(block, previousMaterial, block.Floor, (data) => data.FloorIndexes);
                    this.HandleMaterialChanged(block, previousMaterial, block.Floor, (data) => data.Indexes);
                    break;

                case "Walls":
                    previousMaterial = (Material)this.blockPropertyChange[e.PropertyName];
                    //this.HandleMaterialChanged(block, previousMaterial, block.Walls, (data) => data.WallIndexes);
                    this.HandleMaterialChanged(block, previousMaterial, block.Walls, (data) => data.Indexes);
                    break;

                case "Ceiling":
                    previousMaterial = (Material)this.blockPropertyChange[e.PropertyName];
                    // this.HandleMaterialChanged(block, previousMaterial, block.Ceiling, (data) => data.CeilingIndexes);
                    this.HandleMaterialChanged(block, previousMaterial, block.Ceiling, (data) => data.Indexes);
                    break;

                case "CeilingShape":
                case "FloorShape":
                    var shape = (GameObject)this.blockPropertyChange[e.PropertyName];
                    if (shape != null)
                    {
                        this.PositionShapes(block);
                        this.ScaleShape(shape);
                        this.UpdateShapeRotations(block);
                    }

                    break;

                case "CeilingShapeRotation":
                case "FloorShapeRotation":
                    this.UpdateShapeRotations(block);
                    break;
            }

            this.blockPropertyChange.Remove(e.PropertyName);
        }

        private void HandleMaterialChanged(BlockModel model, Material previous, Material current, Func<MaterialDataModel, Dictionary<Guid, int>> getDictionary)
        {
            // check if the material was something  
            if (previous != null)
            {
                var instanceId = previous.GetInstanceID();
                if (this.materials.ContainsKey(instanceId))
                {
                    var value = this.materials[instanceId];
                    var removed = value.Indexes.Remove(model.UniqueId);

                    // check if removed
                    if (removed)
                    {
                        this.RemoveMaterialData(instanceId);
                    }
                }
            }

            // check if material changed to different material
            if (current != null)
            {
                var data = this.UpdateMeshIndices(current);
                var dictionary = getDictionary(data);
                var count = dictionary.ContainsKey(model.UniqueId) ? dictionary[model.UniqueId] : 0;
                dictionary[model.UniqueId] = count + 1; //new SubMeshMaterialData(material, i));
                // dictionary[model.UniqueId] = true;
            }

            // rebuild indicies
            this.RebuildIndicies();

            // update renderer materials
            this.renderer.sharedMaterials = this.materialArray;
            this.OnUpdatedRendererMaterials();
        }

        /*
           private void HandleMaterialChanged(BlockModel model, Material previous, Material current, Func<MaterialDataModel, Dictionary<Guid, bool>> getDictionary)
        {
            // check if the material was something  
            if (previous != null)
            {
                var instanceId = previous.GetInstanceID();
                if (this.materials.ContainsKey(instanceId))
                {
                    var value = this.materials[instanceId];
                    var removed = value.FloorIndexes.Remove(model.UniqueId) |
                                  value.WallIndexes.Remove(model.UniqueId) |
                                  value.CeilingIndexes.Remove(model.UniqueId);

                    // check if removed
                    if (removed)
                    {
                        this.RemoveMaterialData(instanceId);
                    }
                }
            }

            // check if material changed to different material
            if (current != null)
            {
                var data = this.UpdateMeshIndices(current);
                var dictionary = getDictionary(data);
                dictionary[model.UniqueId] = true;
            }

            // rebuild indicies
            this.RebuildIndicies();

            // update renderer materials
            this.renderer.sharedMaterials = this.materialArray;
            this.OnUpdatedRendererMaterials();
        }
         */

        private void PositionShapes(BlockModel model)
        {
            if (model == null)
            {
                return;
            }

            var shape = model.FloorShape;
            if (shape != null)
            {
                shape.transform.position = new Vector3(model.LocationX, model.FloorTop, model.LocationY);
            }

            shape = model.CeilingShape;
            if (shape != null)
            {
                shape.transform.position = new Vector3(model.LocationX, model.FloorBottom, model.LocationY);
            }
        }

        private void UpdateShapeRotations(BlockModel model)
        {
            if (model == null)
            {
                return;
            }

            var shape = model.FloorShape;
            if (shape != null)
            {
                var rotation = shape.transform.eulerAngles;
                rotation.y = model.FloorShapeRotation;
                shape.transform.eulerAngles = rotation;
            }

            shape = model.CeilingShape;
            if (shape != null)
            {
                var rotation = shape.transform.eulerAngles;
                rotation.y = model.CeilingShapeRotation;
                shape.transform.eulerAngles = rotation;
            }
        }

        private void ScaleShape(GameObject shape)
        {
            if (shape == null)
            {
                return;
            }

            shape.transform.parent = this.transform;
            var meshRenderer = shape.GetComponent<MeshRenderer>();
            var scale = Vector3.one;
            if (meshRenderer != null)
            {
                var size = meshRenderer.bounds.size;
                var max = Math.Max(size.x, size.y);
                var scaling = 1 / max;
                scale = Vector3.one * scaling;
            }

            shape.transform.localScale = scale;
        }

        private MaterialDataModel UpdateMeshIndices(Material material)
        {
            if (material == null)
            {
                throw new ArgumentNullException("material");
            }

            var instanceId = material.GetInstanceID();
            MaterialDataModel data;
            if (this.materials.ContainsKey(instanceId))
            {
                data = this.materials[instanceId];
            }
            else
            {
                // create a new data for the material
                data = new MaterialDataModel();
                data.Material = material;
                data.MaterialIndex = this.materialArray.Length;

                // add the new materials data
                this.materials.Add(instanceId, data);

                // update and assign the materials array
                Array.Resize(ref this.materialArray, this.materialArray.Length + 1);
                this.materialArray[this.materialArray.Length - 1] = data.Material;
                this.renderer.sharedMaterials = this.materialArray;
                this.OnUpdatedRendererMaterials();
            }

            return data;
        }

        public void RebuildIndicies()
        {
#if PERFORMANCE
            var perf = PerformanceTesting<string>.Instance;
            perf.Start(PerformanceConstants.SingleMeshGridMapManager_RebuildIndicies);
#endif

            try
            {
                // set the map mesh sub mesh count to the same number of materials
                this.mapMesh.subMeshCount = this.materialArray.Length;

                // process each material
                for (var i = 0; i < this.materialArray.Length; i++)
                {
                    // get material reference and material data 
                    var material = this.materialArray[i];
                    var data = this.materials[material.GetInstanceID()];
                    var indices = new int[0];

                    // process each indexes from material data
                    foreach (var pair in data.Indexes)
                    {
                        // get a reference to the vertex data using the unique id
                        var vertexData = this.objectTracker[pair.Key];

                        // get the start index where the vertexes start
                        var startIndex = this.meshIndexes[pair.Key];

                        // process each sub mesh in vertex data and add it's indices to the indices array
                        for (var subMeshIndex = 0; subMeshIndex < vertexData.SubMeshCount; subMeshIndex++)
                        {
                            // check if the materials match and of no skip to the next sub mesh
                            var subMeshMaterial = vertexData.Materials[subMeshIndex];
                            if (subMeshMaterial == null || subMeshMaterial.GetInstanceID() != material.GetInstanceID())
                            {
                                continue;
                            }

                            // get indices for the sub mesh  
                            var array = vertexData.GetIndicies(subMeshIndex);

                            // shift vertex indices by the value of start index
                            for (var index = 0; index < array.Length; index++)
                            {
                                array[index] = array[index] + startIndex;
                            }

                            // add indices
                            indices = indices.Add(array);
                        }
                    }

                    // set the map indices for the sub mesh index
                    this.mapMesh.SetIndices(indices, MeshTopology.Triangles, i);
                }

                var collider = this.GetComponent<MeshCollider>();
                collider.sharedMesh = this.mapMesh;
                collider.convex = true;
                collider.convex = false;
            }
            finally
            {
#if PERFORMANCE
                perf.Stop(PerformanceConstants.SingleMeshGridMapManager_RebuildIndicies);
#endif
            }
        }

        /*
               public void RebuildIndicies()
        {
            this.mapMesh.subMeshCount = this.materialArray.Length;
            for (var i = 0; i < this.materialArray.Length; i++)
            {
                var material = this.materialArray[i];
                var data = this.materials[material.GetInstanceID()];
                var indicies = new int[0];

                foreach (var pair in data.FloorIndexes)
                {
                    var startIndex = this.meshIndexes[pair.Key];
                    var floorIndexes = new int[]
                        {
                            startIndex + 4, startIndex + 5, startIndex + 6,      // top
                            startIndex + 6, startIndex + 7, startIndex + 4,      // top
                        };
                    indicies = indicies.Add(floorIndexes);
                }

                foreach (var pair in data.CeilingIndexes)
                {
                    var startIndex = this.meshIndexes[pair.Key];
                    var ceilingIndexes = new int[]
                        {
                            startIndex + 1, startIndex + 0, startIndex + 3,      // bottom
                            startIndex + 3, startIndex + 2, startIndex + 1,      // bottom
                        };
                    indicies = indicies.Add(ceilingIndexes);
                }

                foreach (var pair in data.WallIndexes)
                {
                    var startIndex = this.meshIndexes[pair.Key];
                    var wallsIndexes = new int[]
                        {
                            startIndex + 10, startIndex + 9,  startIndex + 8,    // back
                            startIndex + 11, startIndex + 8,  startIndex + 9,    // back
                                    
                            startIndex + 14, startIndex + 13, startIndex + 12,   // left
                            startIndex + 15, startIndex + 12, startIndex + 13,   // left
                                          
                            startIndex + 18, startIndex + 17, startIndex + 16,   // front
                            startIndex + 19, startIndex + 16, startIndex + 17,   // front
                                          
                            startIndex + 22, startIndex + 21, startIndex + 20,   // right
                            startIndex + 23, startIndex + 20, startIndex + 21,   // right
                        };

                    indicies = indicies.Add(wallsIndexes);
                }

                this.mapMesh.SetIndices(indicies, MeshTopology.Triangles, i);
            }
        }
         */
    }
}